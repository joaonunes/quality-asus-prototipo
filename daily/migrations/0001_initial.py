# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-11-27 01:15
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('comum', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('release', models.IntegerField()),
                ('sprint', models.IntegerField()),
                ('semana', models.IntegerField()),
                ('preenche_daily_diariamente', models.BooleanField(default=False, help_text='A equipe preenche o bot da Daily diariamente', verbose_name='Frequência')),
                ('problemas_impedimentos_relatados', models.BooleanField(default=False, help_text='Problemas e Impedimentos são relatados pela equipe durante a Daily', verbose_name='Problemas e Impedimentos')),
                ('data_criacao', models.DateTimeField(auto_now_add=True, verbose_name='Criado em')),
                ('data_modificacao', models.DateTimeField(auto_now=True, verbose_name='Modificado em')),
                ('criado_por', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, related_name='criado_por', to=settings.AUTH_USER_MODEL)),
                ('projeto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='projeto', to='comum.Projeto')),
            ],
            options={
                'verbose_name': 'Checklist',
                'verbose_name_plural': 'Checklists',
            },
        ),
        migrations.AlterUniqueTogether(
            name='checklist',
            unique_together=set([('projeto', 'release', 'sprint', 'semana')]),
        ),
    ]
