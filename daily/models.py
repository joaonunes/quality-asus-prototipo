from django.db import models
from comum.models import Projeto
from django.contrib.auth.models import User


# Create your models here.
class CheckList(models.Model):
    class Meta:
        verbose_name = u'Checklist'
        verbose_name_plural = u'Checklists'
        unique_together = ('projeto', 'release', 'sprint', 'semana')

    projeto = models.ForeignKey(Projeto, related_name='projeto', on_delete=models.PROTECT)
    release = models.IntegerField(null=False, blank=False)
    sprint = models.IntegerField(null=False, blank=False)
    semana = models.IntegerField(null=False, blank=False)
    criado_por = models.ForeignKey(User, related_name='criado_por', editable=False)
    preenche_daily_diariamente = models.BooleanField(verbose_name=u'Frequência',help_text=u'A equipe preenche o bot da Daily diariamente', default=False)
    problemas_impedimentos_relatados = models.BooleanField(verbose_name=u'Problemas e Impedimentos',help_text=u'Problemas e Impedimentos são relatados pela equipe durante a Daily', default=False)
    data_criacao = models.DateTimeField(auto_now_add=True, verbose_name=u'Criado em')
    data_modificacao = models.DateTimeField(auto_now=True, verbose_name=u'Modificado em')

    def __str__(self):
        return "%s: Sprint %d" % (self.projeto, self.sprint)