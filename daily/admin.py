from django.contrib import admin
from .models import CheckList

# Register your models here.
class DailyCLAdmin(admin.ModelAdmin):
    list_display = ('projeto', 'release','sprint','semana', 'data_criacao', 'criado_por')
    list_filter = ('projeto', 'criado_por', 'data_criacao')
    search_fields = ('projeto__nome', 'criado_por__username', )
    list_display_links = ('projeto', 'release','sprint','semana', 'data_criacao', 'criado_por')

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.criado_por = request.user
            super().save_model(request, obj, form, change)

admin.site.register(CheckList, DailyCLAdmin)