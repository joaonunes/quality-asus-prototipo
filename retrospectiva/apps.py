from django.apps import AppConfig


class RetrospectivaConfig(AppConfig):
    name = 'retrospectiva'
