from django.contrib import admin
from .models import PlanningCheckList, AtaPlanning, ParticipantePlanning

# Register your models here.
class PlanningCLAdmin(admin.ModelAdmin):
    list_display = ('projeto', 'release','sprint','semana', 'data_criacao', 'criado_por')
    list_filter = ('projeto', 'criado_por', 'data_criacao')
    search_fields = ('projeto__nome', 'criado_por__username', )
    list_display_links = ('projeto', 'release','sprint','semana', 'data_criacao', 'criado_por')

    def save_model(self, request, obj, form, change):
        obj.criado_por = request.user
        obj.save()


class ParticipantePlanningInline(admin.StackedInline):
    model = ParticipantePlanning
    extra = 1
    suit_classes = 'suit-tab suit-tab-detalhes'


class AtaPlanningAdmin(admin.ModelAdmin):
    list_display = ('projeto', 'release','sprint','semana', 'data_criacao', 'criado_por')
    list_filter = ('projeto', 'criado_por', 'data_criacao')
    search_fields = ('projeto__nome', 'criado_por__username', )
    list_display_links = ('projeto', 'release','sprint','semana', 'data_criacao', 'criado_por')
    inlines = [ ParticipantePlanningInline ]
    fieldsets=(
        (None, {
            'classes':('suit-tab', 'suit-tab-identificacao', ),
            'fields': ('projeto', 'release', 'sprint', 'semana' )
        }),
        (u'Itens de Interesse', {
            'classes':('suit-tab', 'suit-tab-itens', ),
            'fields': ('inicio_previsto_da_reuniao', 'inicio_da_reuniao', 'termino_previsto_da_reuniao', 'termino_da_reuniao', 'estorias_selecionadas', 'qtd_atividades', 'qtd_story_points', 'velocidade_equipe', ) 
        }),
        (u'Detalhes', {
            'classes':('suit-tab', 'suit-tab-detalhes', ),
            'fields': ('discutido', 'objetivo_da_sprint')
        })
    )

    suit_form_tabs = (('identificacao', u'Sobre a Sprint'), ('itens', 'Itens de Interesse'), ('detalhes', 'Detalhes da Reunião'),)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.criado_por = request.user
            super().save_model(request, obj, form, change)

admin.site.register(PlanningCheckList, PlanningCLAdmin)
admin.site.register(AtaPlanning, AtaPlanningAdmin)