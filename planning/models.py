from django.db import models
from comum.models import Projeto
from django.contrib.auth.models import User


# Create your models here.
class ParticipantePlanning(models.Model):
    class Meta:
        verbose_name = u'Participante'
        verbose_name_plural = u'Participantes'
        
    nome_do_stakeholder = models.CharField(max_length=255)
    planning = models.ForeignKey('AtaPlanning', on_delete=models.CASCADE)


class AtaPlanning(models.Model):
    class Meta:
        verbose_name = u'Ata'
        verbose_name_plural = u'Atas'
        unique_together = ('projeto', 'release', 'sprint', 'semana')
    projeto = models.ForeignKey(Projeto, related_name='%(class)s_projeto', on_delete=models.PROTECT)
    release = models.IntegerField(null=False, blank=False)
    sprint = models.IntegerField(null=False, blank=False)
    semana = models.IntegerField(null=False, blank=False)
    criado_por = models.ForeignKey(User, verbose_name=u'Criado por', related_name='%(class)s_criado_por', editable=False)
    data_criacao = models.DateTimeField(auto_now_add=True, verbose_name=u'Criado em')
    data_modificacao = models.DateTimeField(auto_now=True, verbose_name=u'Modificado em')

    inicio_previsto_da_reuniao = models.DateTimeField(verbose_name = 'Início Previsto')
    inicio_da_reuniao = models.DateTimeField(verbose_name = 'Início da Reunião')
    termino_previsto_da_reuniao = models.DateTimeField(verbose_name = 'Termino Previsto')
    termino_da_reuniao = models.DateTimeField(verbose_name = 'Término da Reunião')

    estorias_selecionadas = models.IntegerField(verbose_name=u'Estórias de Usuário', help_text=u'Quantidade de estórias de usuário selecionadas para a Sprint.')
    qtd_atividades = models.IntegerField(verbose_name=u'Atividades', help_text=u'Quantidade de atividades obtidas na fase 2 da reunião.')
    qtd_story_points = models.IntegerField(verbose_name=u'StoryPoints', help_text=u'Quantidade de storypoints planejados.')
    velocidade_equipe = models.IntegerField(verbose_name=u'Velocidade da Equipe', help_text=u'(Storypoints)')

    discutido = models.TextField(null=True, blank=True, verbose_name=u'O que foi discutido')
    objetivo_da_sprint = models.TextField(null=True, blank=True, verbose_name=u'Objetivo da Sprint')

    def __str__(self):
        return "Ata da Planning (%s: Sprint %d)" % (self.projeto, self.sprint)
         







class PlanningCheckList(models.Model):
    class Meta:
        verbose_name = u'Checklist'
        verbose_name_plural = u'Checklists'
        unique_together = ('projeto', 'release', 'sprint', 'semana')

    projeto = models.ForeignKey(Projeto, related_name='%(class)s_projeto', on_delete=models.PROTECT)
    release = models.IntegerField(null=False, blank=False)
    sprint = models.IntegerField(null=False, blank=False)
    semana = models.IntegerField(null=False, blank=False)
    criado_por = models.ForeignKey(User, verbose_name=u'Criado por', related_name='%(class)s_criado_por', editable=False)
    data_criacao = models.DateTimeField(auto_now_add=True, verbose_name=u'Criado em')
    data_modificacao = models.DateTimeField(auto_now=True, verbose_name=u'Modificado em')

    no_dia_planejado = models.BooleanField(verbose_name=u'Dia da Planning',help_text=u'A equipe realizou o planejamento no dia planejado.', default=False)
    na_hora_planejada  = models.BooleanField(verbose_name=u'Horário da Planning',help_text=u'A equipe realizou o planejamento no horário planejado.', default=False)
    duracao  = models.BooleanField(verbose_name=u'Duração',help_text=u'Duração da reunião foi de 2 horas ou menos.', default=False)
    toda_equipe  = models.BooleanField(verbose_name=u'Participação da Equipe',help_text=u'Todo o time participou da reunião de planejamento.', default=False)
    backlog_atualizado  = models.BooleanField(verbose_name=u'Backlog Atualizado',help_text=u'O backlog do produto está atualizado.', default=False)
    itens_de_melhoria_na_sprint = models.BooleanField(verbose_name=u'Itens de Melhoria', help_text=u'Itens de melhoria identificados na retrospectiva foram adicionados na Sprint.', default=False)
    ordenacao_backlog = models.BooleanField(verbose_name=u'Backlogo do Produto', help_text=u'Itens do backlog do produto ordenados pelo PO utilizando critérios tais como valor de negócio, riscos, tamanho e dependências.', default=False)
    decomposicao_tarefas = models.BooleanField(verbose_name=u'Decomposição de Tarefas', help_text=u'Itens do backlog foram decompostos em tarefas.', default=False)
    panning_poker = models.BooleanField(verbose_name=u'Planning Poker', help_text=u'Itens do Backlog foram estimados por meio do Planning Poker.', default=False)
    disponibilidade_do_po = models.BooleanField(verbose_name=u'Disponibilidade do PO', help_text=u'O Product Owner do projeto estave disponível enquanto o time estava estimando.', default=False)
    revisa_dod = models.BooleanField(verbose_name=u'Revisão do DoD', help_text=u'A equipe revisou e atualizou a definição de pronto.', default=False)
    objetivos_sprint = models.BooleanField(verbose_name=u'Objetivos da Sprint', help_text=u'Os objetivos da Sprint estão claramente definidos.', default=False)
    capacidade_equipe = models.BooleanField(verbose_name=u'Capacidade da Equipe', help_text=u'A capacidade da equipe para a Sprint foi definida.', default=False)
    desempenho_passado = models.BooleanField(verbose_name=u'Desempenho passado', help_text=u'O desempenho passado foi levado em consideração para definir o Backlog da Sprint.', default=False)
    ata_reuniao = models.BooleanField(verbose_name=u'Ata da Reunião', help_text=u'A ata da reunião foi registrada.', default=False)
    ferramenta_controle = models.BooleanField(verbose_name=u'Turmalina', help_text=u'A ferramenta de controle está atualizada.', default=False)

    def __str__(self):
        return "%s: Sprint %d.%d" % (self.projeto, self.sprint, self.semana)