from django.apps import AppConfig


class SprintReviewConfig(AppConfig):
    name = 'sprint_review'
