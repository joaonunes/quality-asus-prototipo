from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class Projeto(models.Model):
    class Meta:
        verbose_name=u'Projeto'
        verbose_name_plural=u'Projetos'

    nome = models.CharField(max_length=255, verbose_name='Nome do Projeto', null=False, blank=False)

    def __str__(self):
        return self.nome


class Stakeholder(models.Model):
    class Meta:
        verbose_name=u'Stakeholder'
        verbose_name_plural=u'Stakeholders'

    nome = models.CharField(max_length=255, null=False, blank=False)
    participacao_em_planning = models.ForeignKey('planning.AtaPlanning', related_name=u'planning_stakeholder', null=True, blank=True)
    
    def __str__(self):
        return self.nome


# class User(AbstractUser):
#     projeto = models.ForeignKey(Projeto, blank=True, null=True, related_name="proj")